package com.example.projectcalculator205150400111005;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView resultHasil, resultAwal;
    MaterialButton buttonPlus, buttonMinus, buttonMultiply, buttonDivide;
    MaterialButton buttonOne, buttonTwo, buttonThree, buttonFour, buttonFive, buttonSix, buttonSeven, buttonEight, buttonNine, buttonZero;
    MaterialButton buttonC, buttonR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultHasil = findViewById(R.id.buttons_hasil);
        resultAwal = findViewById(R.id.buttons_awal);

        assignId(buttonPlus,R.id.button_tambah);
        assignId(buttonMinus,R.id.button_kurang);
        assignId(buttonMultiply,R.id.button_kali);
        assignId(buttonDivide,R.id.button_bagi);
        assignId(buttonOne,R.id.button_satu);
        assignId(buttonTwo,R.id.button_dua);
        assignId(buttonThree,R.id.button_tiga);
        assignId(buttonFour,R.id.button_empat);
        assignId(buttonFive,R.id.button_lima);
        assignId(buttonSix,R.id.button_enam);
        assignId(buttonSeven,R.id.button_tujuh);
        assignId(buttonEight,R.id.button_delapan);
        assignId(buttonNine,R.id.button_sembilan);
        assignId(buttonZero,R.id.button_nol);
        assignId(buttonC,R.id.button_c);
        assignId(buttonR,R.id.button_hasil);

    }

    void assignId(MaterialButton btn,int id) {
        btn = findViewById(id);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MaterialButton button = (MaterialButton) view;
        String buttonText = button.getText().toString();
        String dataToCalculate = resultAwal.getText().toString();

        if(buttonText.equals("C")){
            resultAwal.setText("");
            resultHasil.setText("0");
            return;
        }
        if (buttonText.equals("=")){
            resultAwal.setText(resultHasil.getText());
            return;
        }
        else {
            dataToCalculate = dataToCalculate+buttonText;
        }

        resultAwal.setText(dataToCalculate);

        String finalResult = getResult(dataToCalculate);

        if(!finalResult.equals("Err")){
            resultHasil.setText(finalResult);
        }
    }

    String getResult(String data){
        try {
                Context context = Context.enter();
                context.setOptimizationLevel(-1);
                Scriptable scriptable = context.initStandardObjects();
                String finalResult = context.evaluateString(scriptable,data, "Javascript", 1, null).toString();
                return finalResult;
        }catch (Exception e){
            return "Err";
        }
    }

}